package main

import (
	redistest "redistest/internal/redistest"
)

func main() {
	rt := redistest.New()
	rt.Start()
}
