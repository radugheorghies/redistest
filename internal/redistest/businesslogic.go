package redistest

import (
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"time"
)

func (rt *RedisTest) writeValues() {

	var counter int32
	ch := make(chan struct{})

	go func() {
		ticker := time.NewTicker(time.Second)
		<-ch
		for v := range ticker.C {
			log.Println("Second", v, "Counter:", atomic.LoadInt32(&counter))
			atomic.StoreInt32(&counter, 0)
		}
	}()

	var wg sync.WaitGroup
	// wg.Add(numGrs * numValuesPerBatch)
	// for i := 0; i < numGrs; i++ {
	// 	for j := 0; j < numValuesPerBatch; j++ {
	// 		go func(i, j int) {
	// 			key := fmt.Sprintf("%s:%s:%s:%s:%s:%d:%d", string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), i, j)
	// 			if err := rt.rc.HSet(key, "0", 0).Err(); err != nil {
	// 				log.Println("Error set key", err)
	// 			} else {
	// 				atomic.AddInt32(&counter, 1)
	// 			}
	// 			wg.Done()
	// 		}(i, j)
	// 	}
	// }

	wg.Add(numGrs)
	ch <- struct{}{}
	for i := 0; i < numGrs; i++ {
		go func(i int) {
			for j := 0; j < numValuesPerBatch; j++ {
				key := fmt.Sprintf("%s:%s:%s:%s:%s:%d:%d", string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), string(make([]byte, 20)), i, j)
				if err := rt.rc.HSet(key, "0", 0).Err(); err != nil {
					log.Println("Error set key", err)
				} else {
					atomic.AddInt32(&counter, 1)
				}
			}
			wg.Done()
		}(i)
	}
	wg.Wait()
}
