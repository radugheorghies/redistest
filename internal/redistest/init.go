package redistest

import (
	"time"

	"github.com/go-redis/redis"
)

func (rt *RedisTest) init() {
	rt.connnectToRedis()
}

func (rt *RedisTest) connnectToRedis() {
	rt.rc = redis.NewClient(&redis.Options{
		PoolSize:    100,
		PoolTimeout: 30 * time.Second,
		Addr:        "localhost:6379",
		Password:    "", // no password set
		DB:          0,  // use default DB
	})
}
