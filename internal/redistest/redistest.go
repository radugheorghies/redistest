package redistest

// New is the factory function
func New() *RedisTest {
	return &RedisTest{}
}

// Start will start the app
func (rt *RedisTest) Start() {
	rt.init()
	rt.writeValues()
}
