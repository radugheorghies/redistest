package redistest

import "github.com/go-redis/redis"

const (
	numGrs            = 1000
	numValuesPerBatch = 500
)

// RedisTest is the main package
type RedisTest struct {
	rc *redis.Client
}
